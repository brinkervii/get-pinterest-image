requirements.txt:
	pip-compile requirements.txt.in -o requirements.txt

install:
	cp get-pin.py $$HOME/.local/bin/get-pin
	chmod +x $$HOME/.local/bin/get-pin

.PHONY:
	install