# Download pinterest image

Very simple script. To use just use the terminal command `get-pin <url>` Where `<url>` is the url you copy from your
browser window.

# Install

Just run `make install`.
The Makefile will put the script in `~/.local/bin`. Of course this directory has to be in your `$PATH`

# Dependencies

This script has some common Python dependencies that usually are already present on most Linux distributions. They're
listed in `requirements.txt.in`. `requirements.txt` is a list of frozen requirements that replicates the development
environment.