#!/usr/bin/env python3

from pathlib import Path

import bs4
import click
import requests
from bs4 import BeautifulSoup


def remove_query_parameters(url: str) -> str:
    split = url.split("?")
    return split[0]


def remove_trailing_slash(url: str) -> str:
    return url.rstrip("/")


def clean_url(url: str) -> str:
    url = remove_query_parameters(url)
    url = remove_trailing_slash(url)

    return url


def download_page(url: str) -> BeautifulSoup:
    url = clean_url(url)

    response = requests.get(url)
    response.raise_for_status()

    return BeautifulSoup(response.text, "html5lib")


def link_tag_is_candidate(tag: bs4.Tag):
    has_link_as = "as" in tag.attrs
    is_as_image = has_link_as and tag.attrs["as"] == "image"
    has_href = "href" in tag.attrs

    return has_link_as and is_as_image and has_href


def href_from_link_tag(tag: bs4.Tag):
    assert "href" in tag.attrs
    return tag.attrs["href"]


def find_main_image_url(page: BeautifulSoup) -> str:
    link_tags = list(page.find_all("link"))
    candidate_link_tags = list(filter(link_tag_is_candidate, link_tags))
    image_links = list(set(map(href_from_link_tag, candidate_link_tags)))

    return image_links[0]


def image_name_from_url(url: str) -> str:
    url_split = url.split("/")
    image_name = url_split[-1]

    return remove_query_parameters(image_name)


def download_image_to_cwd(image_link: str) -> Path:
    path = Path(image_name_from_url(image_link)).resolve()

    response = requests.get(image_link)
    response.raise_for_status()

    with path.open("wb+") as fp:
        fp.write(response.content)

    return path


@click.argument("url", type=str)
@click.command()
def cli(url: str):
    page = download_page(url)
    main_image_url = find_main_image_url(page)
    downloaded_to = download_image_to_cwd(main_image_url)

    print(f"Image saved as '{downloaded_to.name}'")


if __name__ == "__main__":
    cli()
